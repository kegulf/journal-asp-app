using journal_asp_app.Models;
using journal_asp_app.Data;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace journal_asp_app.Controllers
{

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]

    public class JournalEntryController : ControllerBase
    {
        private readonly ApplicationDbContext dbContext;
        private readonly UserManager<ApplicationUser> userManager;

        public JournalEntryController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            dbContext = context;
            this.userManager = userManager;

        }

        // CREATE
        [HttpPost] // http://localhost:3000/api/journalentry/
        public async Task<ActionResult<JournalEntry>> Post(JournalEntry journalEntry)
        {
            try
            {
                var user = await userManager.FindByIdAsync(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
                journalEntry.ApplicationUserId = user.Id;
                dbContext.Add(journalEntry);
                dbContext.SaveChanges();

                return CreatedAtAction(
                    "GetJournalEntryById", new JournalEntry { Id = journalEntry.Id }, journalEntry
                );
            }
            catch (Exception e)
            {
                return Unauthorized(e);
            }
        }


        // READ
        [HttpGet("{id:int}")] // http://localhost:3000/api/journalentry/:id

        async public Task<ActionResult<JournalEntry>> GetJournalEntryById(int id)
        {
            try
            {
                var user = await userManager.FindByIdAsync(
                    HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value
                );

                var entry = await dbContext.JournalEntries.Where(e => e.Id == id && e.ApplicationUserId == user.Id).ToListAsync();

                if (entry.Count < 1)
                {
                    return NoContent();
                }

                return Ok(entry);
            }

            catch (Exception e)
            {
                return Unauthorized(e);
            }
        }

        [HttpGet("all")] // http://localhost:3000/api/journalentry/all

        async public Task<ActionResult<IEnumerable<JournalEntry>>> GetJournalEntries()
        {
            try
            {
                var user = await userManager.FindByIdAsync(
                    HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value
                );

                var entries = await dbContext.JournalEntries.Where(
                    e => e.ApplicationUserId == user.Id
                ).ToListAsync();

                return Ok(entries);
            }
            catch (Exception e)
            {
                return Unauthorized(e);
            }
        }

        // UPDATE
        [HttpPut("{id:int}")] // http://localhost:3000/api/journalentry/:id

        public IActionResult UpdateJournalEntry(int id, JournalEntry journalEntry)
        {
            if (id != journalEntry.Id)
            {
                return BadRequest("The id recieved by the API don't match the id of the Entry");
            }

            dbContext.Entry(journalEntry).State = EntityState.Modified;
            dbContext.SaveChanges();

            return Ok(journalEntry);
        }


        // DELETE
        [HttpDelete("{id:int}")] // http://localhost:3000/api/journalentry/:id

        public ActionResult<JournalEntry> DeleteJournalEntry(int id)
        {
            var journalEntryToDelete = dbContext.JournalEntries.Find(id);

            if (journalEntryToDelete == null)
            {
                return NotFound();
            }

            dbContext.JournalEntries.Remove(journalEntryToDelete);
            dbContext.SaveChanges();

            return NoContent();
        }
    }
}
