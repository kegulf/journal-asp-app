using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace journal_asp_app.Models
{
    public class JournalEntry
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string ContentText { get; set; }
        public string CreatedAt { get; set; }
        public string ModifiedAt { get; set; }
        public string ApplicationUserId { get; set; }

        [NotMapped]
        public ApplicationUser ApplicationUser { get; set; }
    }
}

