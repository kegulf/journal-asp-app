export const HOME = "/";
export const LOGIN = "/login";
export const JOURNAL_FEED = "/journal-feed";
export const SINGLE_JOURNAL_ENTRY = "/journal-entry/:id";
