import PropTypes from "prop-types";
import React from "react";

import JournalEntryCard from "./../components/JournalEntryCard/JournalEntryCard";

const JournalEntryCardContainer = ({ journalEntries, onEdit, onDelete }) => (
	<div>
		{journalEntries
			.sort((a, b) => {
				return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
			})
			.map(entry => {
				return (
					<JournalEntryCard
						key={entry.id}
						journalEntry={entry}
						onEditButtonClick={() => onEdit(entry)}
						onDeleteButtonClick={() => onDelete(entry)}
					/>
				);
			})}
	</div>
);

JournalEntryCardContainer.propTypes = {
	journalEntries: PropTypes.array.isRequired,
	onEdit: PropTypes.func.isRequired,
	onDelete: PropTypes.func.isRequired
};

export default JournalEntryCardContainer;
