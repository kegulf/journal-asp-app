import PropTypes from "prop-types";
import React from "react";

import "./ClosingButton.css";

const ClosingButton = ({ onClick }) => (
	<button className="closing-button" onClick={onClick}>
		<div className="line-1"></div>
		<div className="line-2"></div>
	</button>
);

ClosingButton.propTypes = {
	onClick: PropTypes.func.isRequired
};

export default ClosingButton;
