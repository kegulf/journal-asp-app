import React from "react";
import { Link } from "react-router-dom";

import "./JournalEntryCard.css";

const JournalEntryCard = props => {
	const { onEditButtonClick, onDeleteButtonClick, journalEntry } = props;

	const { id, title, subtitle, contentText, createdAt, modifiedAt } = journalEntry;

	let finalText;
	let showLink = false;

	if (contentText.length > 150) {
		finalText = contentText.slice(0, 149) + "...";
		showLink = true;
	} else {
		finalText = contentText;
		showLink = true;
	}

	return (
		<div className="journal-entry-card">
			<button className="delete-button" onClick={() => onDeleteButtonClick()}>
				Delete
			</button>
			<button className="edit-button" onClick={() => onEditButtonClick(id)}>
				Edit
			</button>
			<Link to={"/journal-entry/" + id}>
				<h3>{title}</h3>
			</Link>
			<p>
				<i>{subtitle}</i>
			</p>
			<p>
				Created: {new Date(createdAt).toLocaleString()}
				{createdAt !== modifiedAt ? `, Edited: ${new Date(modifiedAt).toLocaleString()}` : ""}
			</p>
			<p>{finalText}</p>
			<p>{showLink ? <Link to={"/journal-entry/" + id}>Show more</Link> : ""}</p>
		</div>
	);
};

export default JournalEntryCard;
