import PropTypes from "prop-types";
import React from "react";
import Modal from "../Modal/Modal";
import ModalHeader from "../Modal/ModalHeader";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "../Modal/ModalFooter";

const DeleteEntryModal = ({ isVisible, onCloseButtonClick, onOkButtonClick, entry }) => (
	<Modal isVisible={isVisible} onCloseButtonClick={onCloseButtonClick}>
		<ModalHeader title={`Please confirm`} onCloseButtonClick={onCloseButtonClick} />
		<ModalBody>
			<p> Are you sure you want to delete {entry && entry.title}?</p>
		</ModalBody>
		<ModalFooter>
			<button className="btn btn-secondary" onClick={onCloseButtonClick}>
				Cancel
			</button>
			<button className="btn btn-primary" onClick={onOkButtonClick}>
				Yes
			</button>
		</ModalFooter>
	</Modal>
);

DeleteEntryModal.propTypes = {};

export default DeleteEntryModal;
