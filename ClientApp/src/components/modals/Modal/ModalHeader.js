import PropTypes from "prop-types";
import React from "react";

import ClosingButton from "./../../controls/ClosingButton/ClosingButton";

const ModalHeader = ({ title, onCloseButtonClick }) => (
	<section className="modal-header">
		<h3>{title}</h3>
		<ClosingButton onClick={onCloseButtonClick} />
	</section>
);

ModalHeader.propTypes = {
	title: PropTypes.string,
	onCloseButtonClick: PropTypes.func.isRequired
};

export default ModalHeader;
