import React from "react";

const ModalFooter = ({ children }) => <section className="modal-footer">{children}</section>;

export default ModalFooter;
