import PropTypes from "prop-types";
import React from "react";

import "./Modal.css";

const Modal = ({ children, isVisible, onCloseButtonClick }) => {
	const modalStyle = {
		display: isVisible ? "block" : "none"
	};

	return (
		<>
			<div className="backdrop" style={modalStyle} onClick={onCloseButtonClick}></div>

			<div className="modal" style={modalStyle}>
				{children}
			</div>
		</>
	);
};

Modal.propTypes = {
	isVisible: PropTypes.bool.isRequired,
	onCloseButtonClick: PropTypes.func.isRequired
};

export default Modal;
