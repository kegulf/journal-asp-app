import PropTypes from "prop-types";
import React from "react";

import Modal from "./../Modal/Modal";
import ModalHeader from "../Modal/ModalHeader";
import ModalBody from "../Modal/ModalBody";
import ModalFooter from "./../Modal/ModalFooter";

import JournalEntryForm from "./../../forms/JournalEntryForm/JournalEntryForm";

const JournalEntryModal = ({
	isVisible,
	onCloseButtonClick,
	onSave,
	onEdit,
	inputValues,
	onChange,
	isNewEntry
}) => (
	<Modal isVisible={isVisible} onCloseButtonClick={onCloseButtonClick}>
		<ModalHeader
			title={isNewEntry ? "Create entry" : "Edit entry"}
			onCloseButtonClick={onCloseButtonClick}
		/>
		<ModalBody>
			<JournalEntryForm onChange={onChange} inputValues={inputValues} />
		</ModalBody>
		<ModalFooter>
			<button className="btn btn-secondary" onClick={onCloseButtonClick}>
				Cancel
			</button>
			<button className="btn btn-primary" onClick={isNewEntry ? onSave : onEdit}>
				{isNewEntry ? "Create" : "Save"}
			</button>
		</ModalFooter>
	</Modal>
);

JournalEntryModal.propTypes = {};

export default JournalEntryModal;
