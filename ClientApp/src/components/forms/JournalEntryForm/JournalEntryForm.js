import React from "react";

import "./JournalEntryForm.css";

const JournalEntryForm = props => {
	const { onChange, inputValues } = props;

	const { title, subtitle, contentText } = inputValues;

	return (
		<form className="journal-entry-form">
			<p>
				<label htmlFor="title">Title: </label>
				<input type="text" name="title" value={title} onChange={onChange} />
			</p>
			<p>
				<label htmlFor="subtitle">Subtitle:</label>
				<input type="text" name="subtitle" value={subtitle} onChange={onChange} />
			</p>
			<p>
				<label htmlFor="contentText">Text: </label>
				<textarea type="text" name="contentText" value={contentText} onChange={onChange} />
			</p>
		</form>
	);
};

export default JournalEntryForm;
