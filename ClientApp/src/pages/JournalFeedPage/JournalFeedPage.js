import React, { Component } from "react";
import "./JournalFeedPage.css";

import {
	getJournalEntries,
	createJournalEntry,
	updateJournalEntry,
	deleteJournalEntry
} from "./../../dataFetcher/dataFetcher";
import JournalEntryFormModal from "../../components/modals/JournalEntryFormModal/JournalEntryFormModal";

import DeleteEntryModal from "./../../components/modals/DeleteEntryModal/DeleteEntryModal";
import JournalEntryCardContainer from "../../containers/JournalEntryCardContainer";

class JournalFeedPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			journalEntries: null,
			loading: false,
			error: null,

			deleteEntryModalIsOpen: false,
			entryToDelete: null,

			entryModalIsOpen: false,
			isNewEntry: false,
			inputValues: {
				title: "",
				subtitle: "",
				contentText: ""
			}
		};
		this.onJournalEntryFormEditButtonClick = this.onJournalEntryFormEditButtonClick.bind(this);
		this.onJournalEntryCardDeleteButtonClick = this.onJournalEntryCardDeleteButtonClick.bind(
			this
		);
		this.onJournalEntryCardEditButtonClick = this.onJournalEntryCardEditButtonClick.bind(this);
	}

	onInputChange(e) {
		const name = e.target.name;
		const value = e.target.value;

		this.setState(prev => ({
			inputValues: {
				...prev.inputValues,
				[name]: value
			}
		}));
	}

	componentDidMount() {
		// As we start the fetching of the data, we set loading to true
		this.setState({ loading: true });

		getJournalEntries()
			.then(journalEntries => {
				// Data has been loaded, save them in state and set state to not loading
				this.setState({ journalEntries: journalEntries, loading: false });
			})
			.catch(error => {
				console.log("Something went wrong when fetching entries");
				console.log(error);

				this.setState({ loading: false, error });
			});
	}

	onJournalEntryCardEditButtonClick(journalEntry) {
		this.setState({
			inputValues: journalEntry,
			entryModalIsOpen: true,
			isNewEntry: false
		});
	}

	onJournalEntryCardDeleteButtonClick(journalEntry) {
		this.setState({
			deleteEntryModalIsOpen: true,
			entryToDelete: journalEntry
		});
	}

	onJournalEntryFormSaveButtonClick() {
		const createdAt = new Date().toISOString();

		createJournalEntry({
			...this.state.inputValues,
			createdAt,
			modifiedAt: createdAt
		})
			.then(data => {
				this.setState(prev => ({
					journalEntries: [data, ...prev.journalEntries],
					entryModalIsOpen: false
				}));
			})
			.catch(error => console.log(error));
	}

	onJournalEntryFormEditButtonClick(event, entryId) {
		const { inputValues } = this.state;

		const modifiedAt = new Date().toISOString();

		updateJournalEntry(inputValues.id, { ...this.state.inputValues, modifiedAt })
			.then(data => {
				this.setState(prev => {
					const updatedEntries = prev.journalEntries.map(entry =>
						entry.id === inputValues.id ? data : entry
					);

					return {
						journalEntries: updatedEntries,
						entryModalIsOpen: false
					};
				});
			})
			.catch(error => console.log(error));
	}

	render() {
		const {
			loading,
			error,
			journalEntries,

			entryModalIsOpen,
			inputValues,
			isNewEntry,

			deleteEntryModalIsOpen,
			entryToDelete
		} = this.state;

		if (loading) {
			return <h1>Loading...</h1>;
		} else if (error || (!loading && !journalEntries)) {
			return <h1>Something went wrong, could not load your entries</h1>;
		} else {
			return (
				<>
					<JournalEntryFormModal
						isVisible={entryModalIsOpen}
						onCloseButtonClick={() => {
							this.setState({ entryModalIsOpen: false });
						}}
						onChange={this.onInputChange.bind(this)}
						isNewEntry={isNewEntry}
						inputValues={inputValues}
						onSave={this.onJournalEntryFormSaveButtonClick.bind(this)}
						onEdit={this.onJournalEntryFormEditButtonClick}
					/>
					<DeleteEntryModal
						isVisible={deleteEntryModalIsOpen}
						onCloseButtonClick={() =>
							this.setState({ deleteEntryModalIsOpen: false, entryToDelete: null })
						}
						entry={entryToDelete}
						onOkButtonClick={() => {
							deleteJournalEntry(entryToDelete.id)
								.then(
									this.setState(prev => {
										const updatedEntryList = prev.journalEntries.filter(
											entry => entry.id !== entryToDelete.id
										);
										return {
											journalEntries: updatedEntryList,
											entryToDelete: null,
											deleteEntryModalIsOpen: false
										};
									})
								)
								.catch(error => console.log(error));
						}}
					/>
					<section
						style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}
					>
						<h1>Your journal entries</h1>
						<button
							className="create-new-entry-button"
							onClick={() =>
								this.setState({
									isNewEntry: true,
									inputValues: { title: "", subtitle: "", contentText: "" },
									entryModalIsOpen: true
								})
							}
						>
							Add new entry
						</button>
					</section>

					<JournalEntryCardContainer
						onEdit={this.onJournalEntryCardEditButtonClick}
						onDelete={this.onJournalEntryCardDeleteButtonClick}
						journalEntries={journalEntries}
					/>
				</>
			);
		}
	}
}

export default JournalFeedPage;
