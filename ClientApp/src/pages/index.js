import SingleJournalEntryPage from "./SingleJournalEntryPage/SingleJournalEntryPage";
import JournalFeedPage from "./JournalFeedPage/JournalFeedPage";

export { SingleJournalEntryPage, JournalFeedPage };
