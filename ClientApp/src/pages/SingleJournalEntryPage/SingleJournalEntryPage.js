import React, { Component } from "react";
import { getSingleJournalEntry } from "./../../dataFetcher/dataFetcher";
import "./SingleJournalEntryPage.css";

class SingleJournalEntryPage extends Component {
	constructor(props, ...rest) {
		super(props, ...rest);
		this.state = {
			journalEntry: null,
			loading: false,
			error: null
		};
	}

	componentDidMount() {
		const entryId = this.props.match.params.id;

		this.setState({ loading: true });

		getSingleJournalEntry(entryId)
			.then(data => {
				this.setState({ journalEntry: data[0], loading: false });
			})
			.catch(error => {
				this.setState({ loading: false, error });
				console.log(error);
			});
	}

	render() {
		if (this.state.loading) {
			return <p>Loading...</p>;
		} else if (!(this.state.loading || this.state.journalEntry)) {
			return (
				<p>
					An error occured, could not load the entry. <br />
					{this.state.error}
				</p>
			);
		} else {
			return (
				<div className="single-journal-entry">
					<h1 className="title">{this.state.journalEntry.title}</h1>
					<p className="subtitle">{this.state.journalEntry.subtitle}</p>
					<p className="entry">{this.state.journalEntry.contentText}</p>
					{/* <button type="button" onClick={() => onDeleteButtonClick(this.state.journalEntry)}> */}
					<button className="single-page-delete-btn">Delete</button>
					{/* <button type="button" onClick={() => onEditButtonClick(this.state.journalEntry.id)}> */}
					<button className="single-page-edit-btn">Edit</button>
				</div>
			);
		}
	}
}

export default SingleJournalEntryPage;
