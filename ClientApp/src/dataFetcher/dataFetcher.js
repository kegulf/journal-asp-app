import authService from "./../components/api-authorization/AuthorizeService";

export const getJournalEntries = () => {
	return doGetRequest("/api/journalentry/all");
};

export const getSingleJournalEntry = entryId => {
	return doGetRequest("/api/journalentry/" + entryId);
};

export const createJournalEntry = data => {
	return doPostRequest("/api/journalentry", data);
};

export const updateJournalEntry = (entryId, data) => {
	return doPutRequest("/api/journalentry/" + entryId, data);
};

export const deleteJournalEntry = entryId => {
	return doDeleteRequest("/api/journalentry/" + entryId);
};

const doGetRequest = async url => {
	const token = await authService.getAccessToken();

	return fetch(url, {
		method: "GET", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, *cors, same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "same-origin", // include, *same-origin, omit
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${token}`
		},
		redirect: "follow", // manual, *follow, error
		referrer: "no-referrer" // no-referrer, *client
	}).then(res => res.json());
};

const doPostRequest = async (url, data) => {
	const token = await authService.getAccessToken();

	return fetch(url, {
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, *cors, same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "same-origin", // include, *same-origin, omit
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${token}`
		},
		redirect: "follow", // manual, *follow, error
		referrer: "no-referrer", // no-referrer, *client
		body: JSON.stringify(data) // body data type must match "Content-Type" header
	}).then(res => res.json());
};

const doPutRequest = async (url, data) => {
	const token = await authService.getAccessToken();

	return fetch(url, {
		method: "PUT", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, *cors, same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "same-origin", // include, *same-origin, omit
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${token}`
		},
		redirect: "follow", // manual, *follow, error
		referrer: "no-referrer", // no-referrer, *client
		body: JSON.stringify(data) // body data type must match "Content-Type" header
	}).then(res => res.json());
};

const doDeleteRequest = async url => {
	const token = await authService.getAccessToken();

	return fetch(url, {
		method: "DELETE", // *GET, POST, PUT, DELETE, etc.
		mode: "cors", // no-cors, *cors, same-origin
		cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
		credentials: "same-origin", // include, *same-origin, omit
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${token}`
		},
		redirect: "follow", // manual, *follow, error
		referrer: "no-referrer" // no-referrer, *client
	}).then(res => res.json());
};
