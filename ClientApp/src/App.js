import React, { Component } from "react";
import { Route } from "react-router";
import { Layout } from "./components/Layout";
import AuthorizeRoute from "./components/api-authorization/AuthorizeRoute";
import ApiAuthorizationRoutes from "./components/api-authorization/ApiAuthorizationRoutes";
import { ApplicationPaths } from "./components/api-authorization/ApiAuthorizationConstants";

import { JournalFeedPage, SingleJournalEntryPage } from "./pages";

import * as routes from "./constants/routes";

import "./custom.css";

export default class App extends Component {
	static displayName = App.name;

	render() {
		return (
			<Layout>
				<AuthorizeRoute exact path={routes.HOME} component={JournalFeedPage} />
				<AuthorizeRoute
					exact
					path={routes.SINGLE_JOURNAL_ENTRY}
					component={SingleJournalEntryPage}
				/>
				<Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes} />
			</Layout>
		);
	}
}
